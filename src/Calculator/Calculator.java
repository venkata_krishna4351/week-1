package Calculator;

public class Calculator {
    //Return type: what is returning back from method
    //Overloading: Two methods having same number but functionality is different or different parameters
    //static : whenever you create a method or variable with static it creates class level reference
    public Integer addition(Integer number1, Integer number2){
        return number1+number2;
    }

    public Double addition(Double number1, Double number2){
        return number1+number2;
    }

    public Double addition(Integer number1, Double number2){
        return number1+number2;
    }

    public static Double mul(Integer number1, Double number2){
        return number1*number2;
    }
}
