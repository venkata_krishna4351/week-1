package Inheritance;

//Interface: In interface by default every method is abstract by default
public interface Bank {
    void deposit(Double amount);
    void withdraw(Double amount);
    void interest();
    Double getBalance();
}

//Always class name starts with capital letter
//New package or package names should be smaller case, should be a noun
