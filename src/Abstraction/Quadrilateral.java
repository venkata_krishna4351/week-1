package Abstraction;

abstract class Quadrilateral {
    public abstract Double perimeter(Double side1, Double side2);
    public abstract Double area(Double side1, Double side2);
}
