package Inheritance;

public class VenkatMain {
    public static void main(String[] args) {
        Venkat venkat = new Venkat();

        venkat.setAddress("2900 la");
        venkat.setEmail("venkat@gmail.com");
        venkat.setLastName("pidikiti");
        venkat.setSchool("VRSEC");
        venkat.setSection("B");

        System.out.println("Email : "+venkat.getEmail()+ " Sec : "+venkat.getSection());
    }
}
