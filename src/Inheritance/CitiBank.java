package Inheritance;

import Person.Person;

public class CitiBank implements Bank, Something{
    private Double initialDeposit;
    private Double balance;
    private final Double rate = 0.04; //Final: Whenever we intialize it we can never change it again
    private Person person;

    //Parametarized Constructor
    public CitiBank(Person person,  Double initialDeposit){
        this.person = person;
        this.initialDeposit = initialDeposit;
        this.balance = initialDeposit;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Double getInitialDeposit() {
        return initialDeposit;
    }

    public void setInitialDeposit(Double initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getRate() {
        return rate;
    }

    @Override
    public void deposit(Double amount) {
        balance  = balance + amount;
    }

    @Override
    public void withdraw(Double amount) {
        balance = balance - amount;
    }

    @Override
    public void interest() {
        balance = balance + (balance*rate);
    }

    @Override
    public void doSomething() {

    }
}
