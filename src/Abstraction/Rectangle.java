package Abstraction;

public class Rectangle extends Quadrilateral{

    public Double perimeter(Double side1, Double side2){
        return (side1+side2)*2;
    }

    @Override
    public Double area(Double side1, Double side2) {
        return null;
    }
}
