package Person;

//Encapsulation: Person class is wrapping up all the variable of a person
public class Person {
    private String firstName; //private: Can be accessed only to this class
    private String lastName;//public: Can be accessed from anywhere
    private String emailId;


    //Default Constructor
    public Person() {
        this.firstName = "sai";
        this.lastName = "krishna";
        this.emailId = "sai@gmail.com";
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() { //CamelCasing:  firstLetter is always small and post fixed words will be starting with capital
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
