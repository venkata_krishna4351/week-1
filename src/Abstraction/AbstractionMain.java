package Abstraction;

public class AbstractionMain {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.perimeter(2.1, 3.1);
    }
}
