package Inheritance;

//Inheritance: Extending props of an another class keyword: extends
//Multiple inheritance is not possible in java
public class Venkat extends Studnet{
    private String lastName;
    private String address;
    private String email;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
