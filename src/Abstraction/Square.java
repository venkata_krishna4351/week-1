package Abstraction;

public class Square extends Quadrilateral{

    @Override
    public Double perimeter(Double side1, Double side2) {
        return (side1*side2)*2;
    }

    @Override
    public Double area(Double side1, Double side2) {
        return side1*side2;
    }
}
