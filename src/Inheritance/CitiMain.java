package Inheritance;

import Person.Person;

public class CitiMain {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setLastName("pidikiti");
        person1.setFirstName("sai");
        person1.setEmailId("sai@gmail.com");
        CitiBank krishna = new CitiBank( person1, 5000.00);
        System.out.println(krishna.getBalance());
        krishna.withdraw(250.00);
        System.out.println(krishna.getBalance());
        System.out.println(krishna.getPerson().getFirstName());
        krishna.deposit(5000.00);
        System.out.println(krishna.getBalance());
    }
}
